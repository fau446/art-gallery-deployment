import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getArts, addArt } from './services/actions/arts';
import Nav from './components/Nav/Nav';
import Header from './components/Header/Header';
import Gallery from './components/Gallery/Gallery';
import UploadModal from './components/UploadModal/UploadModal';
import { fetchMainAccount } from './services/actions/account';

function MainPage({ isLoggedIn, logOut, loggedInUser }) {
  const [uploadModalOpened, setUploadModalOpened] = useState(false);
  const dispatch = useDispatch();
  const { accountData } = useSelector((state) => state.accountStore);

  useEffect(() => {
    dispatch(getArts());
  }, [dispatch]);

  let uploadTitle = '';
  let uploadImageURL = '';

  function openUploadModal() {
    setUploadModalOpened(true);
  }

  function closeUploadModal() {
    setUploadModalOpened(false);
  }

  function handleUploadFieldChange(e) {
    switch (
      e.target.name // for GeneralSection
    ) {
      case 'title':
        uploadTitle = e.target.value;
        break;
      case 'imageURL':
        uploadImageURL = e.target.value;
        break;
      default:
        break;
    }
    e.preventDefault();
  }

  async function onUploadSubmit(e) {
    e.preventDefault();
    console.log(accountData);
    const newArt = {
      title: `${uploadTitle}`, author: `${accountData.username}`, likes: 0, views: 0, imageURL: `${uploadImageURL}`, comments: [],
    };

    try {
      await dispatch(addArt(newArt));
      await dispatch(getArts());
      await dispatch(fetchMainAccount());
      closeUploadModal();
    } catch (error) {
      console.error('Error adding art:', error);
    }
  }

  return (
    <>
      <Nav isLoggedIn={isLoggedIn} logOut={logOut} openUploadModal={openUploadModal} />
      <UploadModal
        uploadModalOpened={uploadModalOpened}
        closeUploadModal={closeUploadModal}
        onUploadSubmit={onUploadSubmit}
        handleUploadFieldChange={handleUploadFieldChange}
      />
      <Header />
      <Gallery isLoggedIn={isLoggedIn} loggedInUser={loggedInUser} />
    </>
  );
}

export default MainPage;
