import React, { useState } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import MainPage from './MainPage';
import LoginPage from './components/LoginPage/LoginPage';
import SignupPage from './components/SignupPage/SignupPage';
import UserPage from './components/UserPage/UserPage';
import OtherUserPage from './components/OtherUserPage/OtherUserPage';
import { deleteCookieAction, logout } from './services/actions/account';
import { deleteCookie } from './utils/cookie';

function Router() {
  const { accountData, loggedIn } = useSelector((state) => state.accountStore);
  const [, setLoggedInUser] = useState('');
  const dispatch = useDispatch();

  function logIn() {
  }

  function logOut() {
    dispatch(deleteCookieAction);
    deleteCookie('accessToken');
    deleteCookie('refreshToken');
    dispatch(logout());
  }

  function setUser(username) {
    setLoggedInUser(username);
  }

  const router = createBrowserRouter([
    {
      path: '/',
      element: <MainPage isLoggedIn={loggedIn} logOut={logOut} loggedInUser={accountData} />,
    },
    {
      path: '/login',
      element: <LoginPage logIn={logIn} setUser={setUser} />,
    },
    {
      path: '/signup',
      element: <SignupPage logIn={logIn} setUser={setUser} />,
    },
    {
      path: '/userpage',
      element: <UserPage
        isLoggedIn={loggedIn}
        loggedInUser={accountData}
      />,
    },
    {
      path: '/otheruserpage',
      element: <OtherUserPage
        isLoggedIn={loggedIn}
        loggedInUser={accountData}
      />,
    },
  ]);

  return <RouterProvider router={router} />;
}

export default Router;
