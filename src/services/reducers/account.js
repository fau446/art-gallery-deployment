import {
  CREATE_ACCOUNT__REQUEST,
  CREATE_ACCOUNT__FAILURE,
  CREATE_ACCOUNT__SUCCESS,
  LOGIN__REQUEST,
  LOGIN__FAILURE,
  LOGIN__SUCCESS,
  EXIT_SIGNUP,
  EXIT_LOGIN,
  FETCH_MAIN_ACCOUNT__SUCCESS,
  FETCH_MAIN_ACCOUNT__FAILURE,
  FETCH_MAIN_ACCOUNT__REQUEST,
  ADD_VIEWS__REQUEST,
  ADD_VIEWS__SUCCESS,
  ADD_VIEWS__FAILURE,
  ADD_LIKE__SUCCESS,
  ADD_LIKE__FAILURE,
  ADD_LIKE__REQUEST,
  REMOVE_LIKE__REQUEST,
  REMOVE_LIKE__SUCCESS,
  REMOVE_LIKE__FAILURE,
  LOGOUT,
  REFRESH_TOKEN_RESET,
  REFRESH_TOKEN__SUCCESS,
  REFRESH_TOKEN__REQUEST,
  REFRESH_TOKEN__FAILURE,
  DELETE_COOKIE,
  SET_COOKIE,
  FETCH_ACCOUNT__FAILURE,
  FETCH_ACCOUNT__REQUEST,
  FETCH_ACCOUNT__SUCCESS,
} from '../actions/account';

const initialState = {
  accountData: {
    username: '',
  },
  myPageData: {
    username: '',
  },
  accountCreated: false,
  loggedIn: false,
  loading: false,
  error: null,
  refreshTokenRequest: {
    success: false,
    error: false,
  },
  fetchAccountRequest: {
    loading: false,
    error: null,
  },
};

// eslint-disable-next-line default-param-last
export default function accountReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_ACCOUNT__REQUEST:
      return {
        ...state,
        fetchAccountRequest: {
          loading: true,
          error: null,
        },
      };
    case FETCH_ACCOUNT__SUCCESS:
      return {
        ...state,
        myPageData: action.payload,
        fetchAccountRequest: {
          loading: false,
          error: null,
        },
      };
    case FETCH_ACCOUNT__FAILURE:
      return {
        ...state,
        fetchAccountRequest: {
          loading: false,
          error: action.payload,
        },
      };
    case SET_COOKIE: {
      return {
        ...state,
      };
    }
    case DELETE_COOKIE: {
      return {
        ...state,
      };
    }
    case REFRESH_TOKEN__REQUEST: {
      return {
        ...state,
        refreshTokenRequest: {
          success: false,
          error: false,
        },
      };
    }
    case REFRESH_TOKEN__SUCCESS: {
      return {
        ...state,
        refreshTokenRequest: {
          success: true,
          error: false,
        },
      };
    }
    case REFRESH_TOKEN__FAILURE: {
      return {
        ...state,
        refreshTokenRequest: {
          success: false,
          error: true,
        },
      };
    }
    case REFRESH_TOKEN_RESET: {
      return {
        ...state,
        refreshTokenRequest: {
          success: false,
          error: false,
        },
      };
    }
    case LOGOUT:
      return {
        ...state,
        accountData: {
          username: '',
        },
        accountCreated: false,
        loggedIn: false,
        loading: false,
        error: null,
      };
    case CREATE_ACCOUNT__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case CREATE_ACCOUNT__SUCCESS:
      return {
        ...state,
        accountData: {
          ...action.payload,
          createdPosts: [],
          likedPosts: [],
          viewedPosts: [],
        },
        loggedIn: true,
        loading: false,
      };
    case CREATE_ACCOUNT__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case LOGIN__REQUEST:
      return {
        ...state,
        loggedIn: false,
        loading: true,
        error: null,
      };
    case LOGIN__SUCCESS:
      return {
        ...state,
        loggedIn: true,
        loading: false,
      };
    case LOGIN__FAILURE:
      return {
        ...state,
        loading: false,
        loggedIn: false,
        error: action.payload,
      };
    case EXIT_SIGNUP:
      return {
        ...state,
        accountCreated: false,
      };
    case EXIT_LOGIN:
      return {
        ...state,
      };
    case FETCH_MAIN_ACCOUNT__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_MAIN_ACCOUNT__SUCCESS:
      return {
        ...state,
        accountData: action.payload,
        loggedIn: true,
        loading: false,
      };
    case FETCH_MAIN_ACCOUNT__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_VIEWS__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_VIEWS__SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case ADD_VIEWS__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case ADD_LIKE__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_LIKE__SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case ADD_LIKE__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case REMOVE_LIKE__REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case REMOVE_LIKE__SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case REMOVE_LIKE__FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
}
