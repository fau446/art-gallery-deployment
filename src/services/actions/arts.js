import { request } from '../../utils/apiUtils';

export const GET_ARTS__REQUEST = 'GET_ARTS__REQUEST';
export const GET_ARTS__SUCCESS = 'GET_ARTS__SUCCESS';
export const GET_ARTS__FAILURE = 'GET_ARTS__FAILURE';

export const GET_ART_BY_ID__REQUEST = 'GET_ART_BY_ID__REQUEST';
export const GET_ART_BY_ID__SUCCESS = 'GET_ART_BY_ID__SUCCESS';
export const GET_ART_BY_ID__FAILURE = 'GET_ART_BY_ID__FAILURE';

export const ADD_ART__REQUEST = 'ADD_ART__REQUEST';
export const ADD_ART__SUCCESS = 'ADD_ART__SUCCESS';
export const ADD_ART__FAILURE = 'ADD_ART__FAILURE';

export const REMOVE_ART__REQUEST = 'REMOVE_ART__REQUEST';
export const REMOVE_ART__SUCCESS = 'REMOVE_ART__SUCCESS';
export const REMOVE_ART__FAILURE = 'REMOVE_ART__FAILURE';

export const GET_COMMENTS_VIEWS__REQUEST = 'GET_COMMENTS_VIEWS__REQUEST';
export const GET_COMMENTS_VIEWS__SUCCESS = 'GET_COMMENTS_VIEWS__SUCCESS';
export const GET_COMMENTS_VIEWS__FAILURE = 'GET_COMMENTS_VIEWS__FAILURE';

export const POST_COMMENTS_VIEWS__REQUEST = 'POST_COMMENTS_VIEWS__REQUEST';
export const POST_COMMENTS_VIEWS__SUCCESS = 'POST_COMMENTS_VIEWS__SUCCESS';
export const POST_COMMENTS_VIEWS__FAILURE = 'POST_COMMENTS_VIEWS__FAILURE';

export const INCREMENT_VIEWS = 'INCREMENT_VIEWS';
export const INCREMENT_LIKES = 'INCREMENT_LIKES';
export const DECREMENT_LIKES = 'DECREMENT_LIKES';

export const RESET_ERROR = 'RESET_ERROR';

export function resetError() {
  return {
    type: RESET_ERROR,
  };
}

export function incrementViews(id) {
  return {
    type: INCREMENT_VIEWS,
    payload: id,
  };
}

export function incrementLikes(id) {
  return {
    type: INCREMENT_LIKES,
    payload: id,
  };
}

export function decrementLikes(id) {
  return {
    type: DECREMENT_LIKES,
    payload: id,
  };
}

export function getArts() {
  return function (dispatch) {
    dispatch({ type: GET_ARTS__REQUEST });
    request('/arts')
      .then((arts) => dispatch({ type: GET_ARTS__SUCCESS, payload: arts }))
      .catch((err) => dispatch({ type: GET_ARTS__FAILURE, payload: err }));
  };
}

export function getArtById(id) {
  return function (dispatch) {
    dispatch({ type: GET_ART_BY_ID__REQUEST });
    request(`/arts/${id}`)
      .then((art) => dispatch({ type: GET_ART_BY_ID__SUCCESS, payload: art }))
      .catch((err) => dispatch({ type: GET_ART_BY_ID__FAILURE, payload: err }));
  };
}

export function addArt(art) {
  return function (dispatch) {
    dispatch({ type: ADD_ART__REQUEST });
    request('/arts', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(art),
    })
      .then((data) => dispatch({ type: ADD_ART__SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: ADD_ART__FAILURE, payload: err }));
  };
}

export function removeArt(id) {
  return function (dispatch) {
    dispatch({ type: REMOVE_ART__REQUEST });
    request(`/arts/${id}`, {
      method: 'DELETE',
    })
      .then((data) => dispatch({ type: REMOVE_ART__SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: REMOVE_ART__FAILURE, payload: err }));
  };
}

export function getCommentsViews(username) {
  return function (dispatch) {
    dispatch({ type: GET_COMMENTS_VIEWS__REQUEST });
    request(`/account/${username}/viewedPosts`)
      .then((data) => dispatch({ type: GET_COMMENTS_VIEWS__SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: GET_COMMENTS_VIEWS__FAILURE, payload: err }));
  };
}

export function postCommentsViews(username, ID) {
  return function (dispatch) {
    dispatch({ type: POST_COMMENTS_VIEWS__REQUEST });
    request(`/account/${username}/viewedPosts`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ ID }),
    })
      .then((data) => dispatch({ type: POST_COMMENTS_VIEWS__SUCCESS, payload: data }))
      .catch((err) => dispatch({ type: POST_COMMENTS_VIEWS__FAILURE, payload: err }));
  };
}
