import { fetchWithRefresh, request } from '../../utils/apiUtils';
import { getCookie, setCookie } from '../../utils/cookie';

export const CREATE_ACCOUNT__REQUEST = 'CREATE_ACCOUNT__REQUEST';
export const CREATE_ACCOUNT__SUCCESS = 'CREATE_ACCOUNT__SUCCESS';
export const CREATE_ACCOUNT__FAILURE = 'CREATE_ACCOUNT__FAILURE';
export const EXIT_SIGNUP = 'EXIT_SIGNUP';
export const EXIT_LOGIN = 'EXIT_LOGIN';

export const LOGIN__REQUEST = 'LOGIN__REQUEST';
export const LOGIN__SUCCESS = 'LOGIN__SUCCESS';
export const LOGIN__FAILURE = 'LOGIN__FAILURE';

export const FETCH_MAIN_ACCOUNT__REQUEST = 'FETCH_MAIN_ACCOUNT__REQUEST';
export const FETCH_MAIN_ACCOUNT__SUCCESS = 'FETCH_MAIN_ACCOUNT__SUCCESS';
export const FETCH_MAIN_ACCOUNT__FAILURE = 'FETCH_MAIN_ACCOUNT__FAILURE';

export const FETCH_ACCOUNT__REQUEST = 'FETCH_ACCOUNT__REQUEST';
export const FETCH_ACCOUNT__SUCCESS = 'FETCH_ACCOUNT__SUCCESS';
export const FETCH_ACCOUNT__FAILURE = 'FETCH_ACCOUNT__FAILURE';

export const ADD_VIEWS__REQUEST = 'ADD_VIEWS__REQUEST';
export const ADD_VIEWS__SUCCESS = 'ADD_VIEWS__SUCCESS';
export const ADD_VIEWS__FAILURE = 'ADD_VIEWS__FAILURE';

export const ADD_LIKE__REQUEST = 'ADD_LIKE__REQUEST';
export const ADD_LIKE__SUCCESS = 'ADD_LIKE__SUCCESS';
export const ADD_LIKE__FAILURE = 'ADD_LIKE__FAILURE';

export const REMOVE_LIKE__REQUEST = 'REMOVE_LIKE__REQUEST';
export const REMOVE_LIKE__SUCCESS = 'REMOVE_LIKE__SUCCESS';
export const REMOVE_LIKE__FAILURE = 'REMOVE_LIKE__FAILURE';

export const REFRESH_TOKEN__REQUEST = 'REFRESH_TOKEN__REQUEST';
export const REFRESH_TOKEN__SUCCESS = 'REFRESH_TOKEN__SUCCESS';
export const REFRESH_TOKEN__FAILURE = 'REFRESH_TOKEN__FAILURE';
export const REFRESH_TOKEN_RESET = 'REFRESH_TOKEN_RESET';

export const LOGOUT = 'LOGOUT';

export const DELETE_COOKIE = 'DELETE_COOKIE';
export const SET_COOKIE = 'SET_COOKIE';

export function fetchAccount(username) {
  return function (dispatch) {
    dispatch({
      type: FETCH_ACCOUNT__REQUEST,
    });
    request(`/account/${username}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getCookie('accessToken')}`,
      },
    })
      .then((res) => {
        dispatch({
          type: FETCH_ACCOUNT__SUCCESS,
          payload: res,
        });
      })
      .catch(() => {
        dispatch({
          type: FETCH_ACCOUNT__FAILURE,
        });
      });
  };
}

export function setCookieAction() {
  return {
    type: SET_COOKIE,
  };
}

export function deleteCookieAction() {
  return {
    type: DELETE_COOKIE,
  };
}

export function refreshTokenReset() {
  return {
    type: REFRESH_TOKEN_RESET,
  };
}

export function refreshTokenRequest() {
  return function (dispatch) {
    dispatch({
      type: REFRESH_TOKEN__REQUEST,
    });
    request('/refresh', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(getCookie('refreshToken')),
    })
      .then((res) => {
        dispatch(setCookieAction());
        setCookie('refreshToken', res.refreshToken);
        setCookie('accessToken', res.accessToken);
        dispatch({
          type: REFRESH_TOKEN__SUCCESS,
          payload: res,
        });
      })
      .catch(() => {
        dispatch({
          type: REFRESH_TOKEN__FAILURE,
        });
      });
  };
}

export function logout() {
  return function (dispatch) {
    dispatch({ type: LOGOUT });
  };
}

export function createAccount(account) {
  return function (dispatch) {
    dispatch({ type: CREATE_ACCOUNT__REQUEST });
    request('/create-account', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(account),
    })
      .then((data) => {
        dispatch(setCookieAction());
        setCookie('refreshToken', data.refreshToken);
        setCookie('accessToken', data.accessToken);
        dispatch({ type: CREATE_ACCOUNT__SUCCESS, payload: data.account });
      })
      .catch((err) => dispatch({ type: CREATE_ACCOUNT__FAILURE, payload: err }));
  };
}

export function exitSignup() {
  return function (dispatch) {
    dispatch({ type: EXIT_SIGNUP });
  };
}

export function exitLogin() {
  return function (dispatch) {
    dispatch({ type: EXIT_LOGIN });
  };
}

export function fetchMainAccount() {
  return async function (dispatch) {
    dispatch({
      type: FETCH_MAIN_ACCOUNT__REQUEST,
    });
    await fetchWithRefresh('/account/me', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${getCookie('accessToken')}`,
      },
    }, refreshTokenRequest, dispatch)
      .then((res) => {
        dispatch({
          type: FETCH_MAIN_ACCOUNT__SUCCESS,
          payload: res,
        });
      })
      .catch(() => {
        dispatch({
          type: FETCH_MAIN_ACCOUNT__FAILURE,
        });
      });
  };
}

export function addViewedPost(username, artId) {
  return function (dispatch) {
    dispatch({ type: ADD_VIEWS__REQUEST });
    console.log('Request ID', artId);
    request(`/account/${username}/viewedPosts`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ID: artId }),
    })
      .then((data) => {
        dispatch({ type: ADD_VIEWS__SUCCESS, payload: data });
        dispatch(fetchMainAccount());
      })
      .catch((err) => {
        dispatch({ type: ADD_VIEWS__FAILURE, payload: err });
        dispatch(fetchMainAccount());
      });
  };
}

export function addLikedPost(username, artId) {
  return function (dispatch) {
    dispatch({ type: ADD_LIKE__REQUEST });
    request(`/account/${username}/likedPosts`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ID: artId }),
    })
      .then((data) => {
        dispatch({ type: ADD_LIKE__SUCCESS, payload: data });
        dispatch(fetchMainAccount());
      })
      .catch((err) => {
        dispatch({ type: ADD_LIKE__FAILURE, payload: err });
        dispatch(fetchMainAccount());
      });
  };
}

export function removeLikedPost(username, artId) {
  return function (dispatch) {
    dispatch({ type: REMOVE_LIKE__REQUEST });
    request(`/account/${username}/likedPosts/${artId}`, {
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })
      .then((data) => {
        dispatch({ type: REMOVE_LIKE__SUCCESS, payload: data });
        dispatch(fetchMainAccount());
      })
      .catch((err) => {
        dispatch({ type: REMOVE_LIKE__FAILURE, payload: err });
        dispatch(fetchMainAccount());
      });
  };
}

export function login(account) {
  return function (dispatch) {
    dispatch({ type: LOGIN__REQUEST });
    request('/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(account),
    })
      .then((data) => {
        dispatch(setCookieAction());
        setCookie('refreshToken', data.refreshToken);
        setCookie('accessToken', data.accessToken);
        dispatch({ type: LOGIN__SUCCESS, payload: data });
      }).then(async () => {
        await dispatch(fetchMainAccount());
      })
      .catch((err) => dispatch({ type: LOGIN__FAILURE, payload: err }));
  };
}
