import {
  combineReducers,
} from 'redux';
import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import artsReducer from './reducers/arts';
import accountReducer from './reducers/account';

const rootReducer = combineReducers({
  artsStore: artsReducer,
  accountStore: accountReducer,
});

const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: [thunk],
});

export default store;
