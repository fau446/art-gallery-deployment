/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styles from './Nav.module.css';
import { getCookie } from '../../utils/cookie';
import { fetchMainAccount } from '../../services/actions/account';

function Nav({
  isLoggedIn, userPageNav = false, logOut, openUploadModal,
}) {
  const { accountData, loggedIn } = useSelector((state) => state.accountStore);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('accountData', accountData.username === '' && getCookie('accessToken'));
    if (accountData.username === '' && getCookie('accessToken')) {
      dispatch(fetchMainAccount());
    }
  }, [loggedIn]);

  if (userPageNav) {
    return (
      <nav className={styles.nav}>
        <Link to="/">Home Page</Link>
      </nav>
    );
  }

  return (
    <nav className={styles.nav}>
      {isLoggedIn ? (
        <>
          <p onClick={openUploadModal} className={styles.p}>Upload</p>
          <Link to="userpage"><p>My Page</p></Link>
          <p onClick={logOut} className={styles.p}>Log out</p>
        </>
      ) : (
        <Link to="login">Login/Sign Up</Link>
      )}
    </nav>
  );
}

export default Nav;
