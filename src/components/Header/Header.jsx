import React, { useEffect } from 'react';
import styles from './Header.module.css';

function Header() {
  useEffect(() => {
  }, []);

  return (
    <div className={styles.header}>
      <h1 className={styles.h1}>Art Gallery</h1>
    </div>
  );
}

export default Header;
