/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/style-prop-object */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import MoonLoader from 'react-spinners/MoonLoader';
import { getArts } from '../../services/actions/arts';
import { addComment } from '../../utils/api';
import styles from './UserPage.module.css';
import Nav from '../Nav/Nav';
import ItemFrame from '../ItemFrame/ItemFrame';
import Modal from '../modal/Modal';

function UserPage({ isLoggedIn, loggedInUser }) {
  const { arts } = useSelector((state) => state.artsStore);
  const {
    accountData,
    myPageData,
    // fetchAccountRequest,
  } = useSelector((state) => state.accountStore);
  const dispatch = useDispatch();
  const location = useLocation();
  const passedAccountData = location.state?.author;
  const [displayedAccountData, setDisplayedAccountData] = useState();
  // state to see if on 'My Posts' or 'Liked Posts'. 0 for 'My Posts', 1 for 'Liked Posts'
  const [currentTab, setCurrentTab] = useState(0);

  const [activeModelIndex, setActiveModelIndex] = useState(-1);
  const [textareaValue, setTextAreaValue] = useState('');

  const ownArts = [];
  const likedArts = [];

  useEffect(() => {
    console.log('1st condition', passedAccountData && myPageData.username !== '');
    if ((passedAccountData && passedAccountData === accountData.username)
       || (!passedAccountData && accountData.username !== '')) {
      setDisplayedAccountData(accountData);
    } else if (passedAccountData && passedAccountData === myPageData.username) {
      setDisplayedAccountData(myPageData);
    }
    console.log('2nd condition', !passedAccountData && accountData.username === '');
    dispatch(getArts());
  }, [dispatch, myPageData, passedAccountData]);

  function openModal(e) {
    setActiveModelIndex(Number(e.target.dataset.id));
  }

  function closeModal() {
    setActiveModelIndex(-1);
  }

  function handleKeyPress(e) {
    if (e.key === 'Enter' || e.key === ' ') {
      openModal();
    }
  }

  /* TODO: There are duplicate handleTextAreaChange and handleCommentSubmit in
  Gallery.jsx and UserPage.jsx. Should move them to Router.jsx
  */

  function handleTextareaChange(e) {
    setTextAreaValue(e.target.value);
  }

  async function handleCommentSubmit(e, artId) {
    if (textareaValue === '') return;
    console.log('loggedInUser:', loggedInUser);
    const comment = { username: `${loggedInUser.username}`, comment: `${textareaValue}`, profilepic: 'https://picsum.photos/500/500' };
    try {
      await addComment(artId, comment);
      dispatch(getArts());
      setTextAreaValue('');
    } catch (error) {
      console.error('Error:', error);
    }
  }

  function switchTabs(e) {
    switch (e.target.innerText) {
      case 'Created Posts':
        setCurrentTab(0);
        break;
      case 'Liked Posts':
        setCurrentTab(1);
        break;
      default:
        break;
    }
  }
  console.log('displayedAccountData:', displayedAccountData);
  if (displayedAccountData) {
    // used to filter which posts the user posted
    for (let i = 0; i < displayedAccountData.createdPosts.length; i += 1) {
      for (let j = 0; j < arts.length; j += 1) {
        if (arts[j].ID === Number(displayedAccountData.createdPosts[i])) {
          ownArts.push(arts[j]);
        }
      }
    }

    // used to filter which posts the user liked
    for (let i = 0; i < displayedAccountData.likedPosts.length; i += 1) {
      for (let j = 0; j < arts.length; j += 1) {
        if (arts[j].ID === Number(displayedAccountData.likedPosts[i])) {
          likedArts.push(arts[j]);
        }
      }
    }
  } else {
    return (
      <>
        <Nav userPageNav />
        <div className={styles.spinner}>
          <MoonLoader
            color="rgb(133, 133, 173, 1)"
            cssOverride={{}}
            loading
            size={100}
            speedMultiplier={1}
          />
        </div>
      </>
    );
  }
  return (
    <div>
      <Nav userPageNav />
      <div className={styles.user_info}>
        <div className={styles.user_stuff}>
          <div className={styles.circular}>
            <img src="https://picsum.photos/1920/1080" alt="Profile Pic" className={styles.profile_image} />
          </div>
          <h1 className={styles.h1}>{displayedAccountData.username}</h1>
        </div>

        <div className={styles.container}>
          <div className={styles.clickable}>
            {currentTab
              ? (
                <>
                  <a className={styles.inactive} onClick={switchTabs}>Created Posts</a>
                  <a className={styles.active} onClick={switchTabs}>Liked Posts</a>
                </>
              )
              : (
                <>
                  <a className={styles.active} onClick={switchTabs}>Created Posts</a>
                  <a className={styles.inactive} onClick={switchTabs}>Liked Posts</a>
                </>
              )}
          </div>
        </div>
        <div className={styles.arts}>
          {currentTab
            ? displayedAccountData.likedPosts.map((item, index) => (
              <div key={item.id}>
                <ItemFrame
                  title={item.title}
                  author={item.author}
                  views={item.views}
                  likes={item.likes}
                  numOfComments={item.comments.length}
                  imageURL={item.imageURL}
                  openModal={openModal}
                  handleKeyPress={handleKeyPress}
                  id={index}
                />
                <Modal
                  title={item.title}
                  author={item.author}
                  isOpen={activeModelIndex === index}
                  views={item.views}
                  likes={item.likes}
                  imageURL={item.imageURL}
                  comments={item.comments}
                  closeModal={closeModal}
                  handleTextareaChange={handleTextareaChange}
                  handleCommentSubmit={handleCommentSubmit}
                  textareaValue={textareaValue}
                  isLoggedIn={isLoggedIn}
                  artID={item.ID}
                />
              </div>
            ))
            : displayedAccountData.createdPosts.map((item, index) => (
              <div key={item.id}>
                <ItemFrame
                  title={item.title}
                  author={item.author}
                  views={item.views}
                  likes={item.likes}
                  numOfComments={item.comments.length}
                  imageURL={item.imageURL}
                  openModal={openModal}
                  handleKeyPress={handleKeyPress}
                  id={index}
                />
                <Modal
                  title={item.title}
                  author={item.author}
                  isOpen={activeModelIndex === index}
                  views={item.views}
                  likes={item.likes}
                  comments={item.comments}
                  imageURL={item.imageURL}
                  closeModal={closeModal}
                  handleTextareaChange={handleTextareaChange}
                  handleCommentSubmit={handleCommentSubmit}
                  textareaValue={textareaValue}
                  isLoggedIn={isLoggedIn}
                  artID={item.ID}
                />
              </div>
            ))}
        </div>
      </div>
    </div>
  );
}

export default UserPage;
