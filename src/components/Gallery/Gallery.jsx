import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import MoonLoader from 'react-spinners/MoonLoader';
import ItemFrame from '../ItemFrame/ItemFrame';
import Modal from '../modal/Modal';
import styles from './Gallery.module.css';
import { getArts, incrementViews } from '../../services/actions/arts';
import { addComment } from '../../utils/api';
import { addViewedPost } from '../../services/actions/account';

function Gallery({ isLoggedIn, loggedInUser }) {
  const { isLoading, arts, getArtsRequest } = useSelector((state) => state.artsStore);
  const { accountData } = useSelector((state) => state.accountStore);
  const dispatch = useDispatch();
  const [updateCounter, setUpdateCounter] = useState(0);
  const [activeModelIndex, setActiveModelIndex] = useState(-1);
  const [textareaValue, setTextAreaValue] = useState('');

  useEffect(() => {
  }, [dispatch, updateCounter]);

  function openModal(e, artID) {
    if (accountData
        && accountData.viewedPosts
        && !accountData.viewedPosts.some((post) => post.ID === artID)) {
      dispatch(incrementViews(artID));
      dispatch(addViewedPost(loggedInUser.username, artID));
      setUpdateCounter((prev) => prev + 1);
    }
    setActiveModelIndex(Number(e.target.dataset.id));
  }

  function closeModal() {
    setActiveModelIndex(-1);
  }

  function handleKeyPress(e) {
    if (e.key === 'Enter' || e.key === ' ') {
      openModal();
    }
  }

  function handleTextareaChange(e) {
    setTextAreaValue(e.target.value);
  }

  async function handleCommentSubmit(e, artId) {
    if (textareaValue === '') return;
    console.log(loggedInUser);
    const comment = { username: `${loggedInUser.username}`, comment: `${textareaValue}`, profilepic: 'https://picsum.photos/500/500' };
    try {
      await addComment(artId, comment);
      dispatch(getArts());
      setTextAreaValue('');
    } catch (error) {
      console.error('Error:', error);
    }
  }

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (getArtsRequest.loading) {
    return (
      <div className={styles.spinner}>
        <MoonLoader
          color="rgb(133, 133, 173, 1)"
          cssOverride={{}}
          loading
          size={100}
          speedMultiplier={1}
        />
      </div>
    );
  }

  return (
    <div className={styles.gallery}>
      {arts.map((item, index) => (
        <div key={item.ID}>
          <ItemFrame
            title={item.title}
            author={item.author}
            views={item.views}
            likes={item.likes}
            numOfComments={item.comments.length}
            imageURL={item.imageURL}
            openModal={openModal}
            handleKeyPress={handleKeyPress}
            id={index}
            artID={item.ID}
          />
          <Modal
            title={item.title}
            author={item.author}
            isOpen={activeModelIndex === index}
            views={item.views}
            likes={item.likes}
            comments={item.comments}
            imageURL={item.imageURL}
            closeModal={closeModal}
            handleTextareaChange={handleTextareaChange}
            handleCommentSubmit={handleCommentSubmit}
            textareaValue={textareaValue}
            isLoggedIn={isLoggedIn}
            loggedInUser={loggedInUser}
            artID={item.ID}
          />
        </div>
      ))}
    </div>
  );
}

export default Gallery;
