/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import styles from './Modal.module.css';
import Comment from '../Comment/Comment';
import { addLikedPost, fetchAccount, removeLikedPost } from '../../services/actions/account';
import { decrementLikes, incrementLikes } from '../../services/actions/arts';
import thumbsUpOutline from '../../images/notLiked.png';
import thumbsUp from '../../images/liked.png';
import { fetchAccountTmp } from '../../utils/api';

function Modal({
  title,
  author,
  views,
  likes,
  comments,
  imageURL,
  isOpen,
  closeModal,
  handleTextareaChange,
  handleCommentSubmit,
  textareaValue,
  isLoggedIn,
  loggedInUser,
  artID,
}) {
  const dispatch = useDispatch();
  const [, setUserData] = React.useState({});
  const [, setLoggedInUserData] = useState({});
  const accountData = useSelector((state) => state.accountStore.accountData);
  const isPostLiked = accountData && accountData.likedPosts
    ? accountData.likedPosts.some((post) => post.ID === artID)
    : false;
  // liked state mainly used for stlyling purposes
  const [liked, setLiked] = useState(isPostLiked);
  const navigate = useNavigate();

  useEffect(() => {
    // TODO: create authors account
    setUserData(fetchAccountTmp(author));
  }, []);

  // TODO: fetch logged in user's account
  useEffect(() => {
    setLoggedInUserData(fetchAccountTmp(author));
  }, []);

  function likePost() {
    console.log('loggedInUser:', loggedInUser);
    if (liked) {
      setLiked(false);
      dispatch(decrementLikes(artID));
      dispatch(removeLikedPost(loggedInUser.username, artID));
    } else {
      setLiked(true);
      dispatch(incrementLikes(artID));
      dispatch(addLikedPost(loggedInUser.username, artID));
    }
  }

  const onClick = async () => {
    dispatch(fetchAccount(author));
    console.log('author:', author);
    navigate('/userpage', { state: { author } });
  };

  if (!isOpen) {
    return null;
  }

  // authorUserInfo being passed through <Link>
  return (
    <div>
      <div className={styles.overlay} onClick={closeModal} />
      <div className={styles.modal}>
        <div className={styles.top}>
          <button className={styles.close} type="button" onClick={closeModal}>X</button>
        </div>
        <div className={styles.content}>
          <div className={styles.left}>
            <h2 className={styles.white}>{title}</h2>
            <div onClick={onClick}>
              <p className={styles.author_name}>
                By:
                {' '}
                {author}
              </p>
            </div>
            <div className={styles.flex}>
              <p className={styles.white}>
                Views:
                {' '}
                {views}
              </p>
              <p className={styles.white}>
                Likes:
                {' '}
                {likes}
              </p>
            </div>
            <div className={styles.comments}>
              {comments.map((comment) => (
                <Comment
                  username={comment.username}
                  comment={comment.comment}
                  profilePic={comment.profilePic}
                />
              ))}
            </div>
            {isLoggedIn && (
              <div className={styles.commentbox}>
                <textarea
                  value={textareaValue}
                  onChange={handleTextareaChange}
                  placeholder="Type in your comment"
                />
                <button className={styles.commentSubmit} type="button" onClick={(e) => handleCommentSubmit(e, artID)} data-id={artID}>Submit</button>
                {liked
                  ? (
                    <button className={styles.like} type="button" onClick={likePost}>
                      <img className={styles.thumbsup} src={thumbsUp} alt="Unlike" />
                    </button>
                  )
                  : (
                    <button className={styles.like} type="button" onClick={likePost}>
                      <img className={styles.thumbsup} src={thumbsUpOutline} alt="Like" />
                    </button>
                  )}
              </div>
            )}
          </div>
          <div className={styles.right}>
            <img className={styles.img} src={imageURL} alt="Post" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
