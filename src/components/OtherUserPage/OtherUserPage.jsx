/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/style-prop-object */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { getArts } from '../../services/actions/arts';
import { addComment } from '../../utils/api';
import styles from './OtherUserPage.module.css';
import Nav from '../Nav/Nav';
import ItemFrame from '../ItemFrame/ItemFrame';
import Modal from '../modal/Modal';

function OtherUserPage({ isLoggedIn, loggedInUser }) {
  const { isLoading, arts } = useSelector((state) => state.artsStore);
  const dispatch = useDispatch();
  // state to see if on 'My Posts' or 'Liked Posts'. 0 for 'My Posts', 1 for 'Liked Posts'
  const [currentTab, setCurrentTab] = useState(0);

  const [activeModelIndex, setActiveModelIndex] = useState(-1);
  const [textareaValue, setTextAreaValue] = useState('');

  const location = useLocation();
  const { otherUserData } = location.state;

  const ownArts = [];
  const likedArts = [];

  useEffect(() => {
    console.log('OtherUserPage.jsx: useEffect');
    dispatch(getArts());
  }, [dispatch]);

  function openModal(e) {
    setActiveModelIndex(Number(e.target.dataset.id));
  }

  function closeModal() {
    setActiveModelIndex(-1);
  }

  function handleKeyPress(e) {
    if (e.key === 'Enter' || e.key === ' ') {
      openModal();
    }
  }

  /* TODO: There are duplicate handleTextAreaChange and handleCommentSubmit in
  Gallery.jsx and UserPage.jsx. Should move them to Router.jsx
  */

  function handleTextareaChange(e) {
    setTextAreaValue(e.target.value);
  }

  async function handleCommentSubmit(e, artId) {
    if (textareaValue === '') return;
    console.log(loggedInUser);
    const comment = { username: `${loggedInUser.username}`, comment: `${textareaValue}`, profilepic: 'https://picsum.photos/500/500' };
    try {
      await addComment(artId, comment);
      dispatch(getArts());
      setTextAreaValue('');
    } catch (error) {
      console.error('Error:', error);
    }
  }

  function switchTabs(e) {
    switch (e.target.innerText) {
      case 'Created Posts':
        setCurrentTab(0);
        break;
      case 'Liked Posts':
        setCurrentTab(1);
        break;
      default:
        break;
    }
  }

  // used to filter which posts the user posted
  for (let i = 0; i < otherUserData.created_posts.length; i += 1) {
    for (let j = 0; j < arts.length; j += 1) {
      if (arts[j].ID === Number(otherUserData.created_posts[i])) {
        ownArts.push(arts[j]);
      }
    }
  }

  // used to filter which posts the user liked
  for (let i = 0; i < otherUserData.liked_posts.length; i += 1) {
    for (let j = 0; j < arts.length; j += 1) {
      if (arts[j].ID === Number(otherUserData.liked_posts[i])) {
        likedArts.push(arts[j]);
      }
    }
  }

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <Nav userPageNav />
      <div className={styles.user_info}>
        <div className={styles.user_stuff}>
          <h1>{otherUserData.username}</h1>
          <img src={otherUserData.profilePic} alt="Profile Pic" className={styles.profile_image} />
        </div>

        <div className={styles.container}>
          <div className={styles.clickable}>
            <a className={styles.link} onClick={switchTabs}>Created Posts</a>
            <a className={styles.link} onClick={switchTabs}>Liked Posts</a>
          </div>
        </div>

        {currentTab
          ? likedArts.map((item, index) => (
            <div key={item.id}>
              <ItemFrame
                views={item.views}
                likes={item.likes}
                numOfComments={item.comments.length}
                imageURL={item.imageURL}
                openModal={openModal}
                handleKeyPress={handleKeyPress}
                id={index}
              />
              <Modal
                title={item.title}
                author={item.author}
                isOpen={activeModelIndex === index}
                views={item.views}
                likes={item.likes}
                comments={item.comments}
                imageURL={item.imageURL}
                closeModal={closeModal}
                handleTextareaChange={handleTextareaChange}
                handleCommentSubmit={handleCommentSubmit}
                textareaValue={textareaValue}
                isLoggedIn={isLoggedIn}
                artID={item.ID}
              />
            </div>
          ))
          : ownArts.map((item, index) => (
            <div key={item.id}>
              <ItemFrame
                views={item.views}
                likes={item.likes}
                numOfComments={item.comments.length}
                imageURL={item.imageURL}
                openModal={openModal}
                handleKeyPress={handleKeyPress}
                id={index}
              />
              <Modal
                title={item.title}
                author={item.author}
                isOpen={activeModelIndex === index}
                views={item.views}
                likes={item.likes}
                comments={item.comments}
                imageURL={item.imageURL}
                closeModal={closeModal}
                handleTextareaChange={handleTextareaChange}
                handleCommentSubmit={handleCommentSubmit}
                textareaValue={textareaValue}
                isLoggedIn={isLoggedIn}
                artID={item.ID}
              />
            </div>
          ))}

      </div>
    </div>
  );
}

export default OtherUserPage;
