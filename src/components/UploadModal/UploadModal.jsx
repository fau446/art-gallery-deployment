import React from 'react';
import styles from './UploadModal.module.css';

function UploadModal({
  uploadModalOpened, closeUploadModal, onUploadSubmit, handleUploadFieldChange,
}) {
  if (!uploadModalOpened) {
    return null;
  }

  return (
    <div>
      <div className={styles.overlay} />
      <div className={styles.modal}>
        <div className={styles.top}>
          <button className={styles.close} onClick={closeUploadModal} type="button">X</button>
        </div>
        <div className={styles.header}>
          <h1>Upload a Work of Art</h1>
        </div>
        <div className={styles.content}>
          <form className={styles.form} onSubmit={onUploadSubmit}>
            <label className={styles.label} htmlFor="title">
              Title:
              <input type="text" id="title" name="title" onChange={handleUploadFieldChange} />
            </label>
            <label className={styles.label} htmlFor="imageURL">
              ImageURL:
              <input type="text" id="imageURL" name="imageURL" onChange={handleUploadFieldChange} />
            </label>
            <button className={styles.submit} type="submit">Upload</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UploadModal;
