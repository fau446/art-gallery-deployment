import React, { useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import styles from './Signup.module.css';
import { createAccount, exitSignup } from '../../services/actions/account';

function SignupPage({ setUser }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { accountData, loggedIn } = useSelector((state) => state.accountStore);

  async function submitForm(e) {
    e.preventDefault();
    const username = e.target[0].value;
    const password = e.target[1].value;
    dispatch(createAccount({ username, password }));
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 1000);
    });
    setUser(username);
  }

  useEffect(() => {
    if (loggedIn && accountData.username !== '') {
      console.log('account created');
      dispatch(exitSignup());
      navigate('/');
    }
  }, [accountData]);

  return (
    <div className={styles.background}>
      <div className={styles.login}>
        {loggedIn && accountData.username !== '' && (
          <p className={styles.success}>Account creation successful!</p>
        )}
        <h1>Create an Account</h1>
        <form onSubmit={submitForm}>
          <label htmlFor="username">
            Username:
            <input type="text" id="username" name="username" required />
          </label>
          <label htmlFor="password">
            Password:
            <input type="password" id="password" name="password" required />
          </label>
          <input className={styles.submit} type="submit" value="Sign Up" />
        </form>
        <Link to="/login"><input className={styles.createbtn} type="submit" value="Already Have an Account?" /></Link>
        <Link to="/"><input className={styles.homebtn} type="submit" value="Back to Homepage" /></Link>
      </div>
    </div>
  );
}

export default SignupPage;
